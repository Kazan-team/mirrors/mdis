__all__ = [
    "Walker",
    "WalkerMeta",
]

import enum
import dataclasses

from . import dispatcher


class WalkerMeta(dispatcher.DispatcherMeta):
    pass


class Walker(dispatcher.Dispatcher, metaclass=WalkerMeta):
    @dispatcher.Hook(tuple, list)
    def dispatch_ordered_sequence(self, node):
        for (index, item) in enumerate(node):
            yield index
            yield item

    @dispatcher.Hook(set, frozenset)
    def dispatch_unordered_sequence(self, node):
        for item in node:
            yield item

    @dispatcher.Hook(dataclasses.is_dataclass)
    def dispatch_dataclass(self, node):
        for field in dataclasses.fields(node):
            key = field.name
            value = getattr(node, key)
            yield key
            yield value

    @dispatcher.Hook(dict)
    def dispatch_mapping(self, node):
        for (key, value) in node.items():
            yield key
            yield value

    @dispatcher.Hook(object)
    def dispatch_object(self, node):
        yield from ()
