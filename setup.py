from setuptools import setup

project = {
    "name": "mdis",
    "author": "Dmitry Selyutin",
    "author_email": "ghostmansd@gmail.com",
    "version": "0.5.2",
    "keywords": [
        "dispatch",
        "dispather",
        "map",
        "iterator",
        "iterable",
        "visitor",
        "walker",
    ],
    "description": "Python dispatching library",
    "classifiers": [
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "Topic :: Software Development :: Libraries",
        "Topic :: Software Development :: Libraries :: Python Modules",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3 :: Only",
        "License :: OSI Approved :: BSD License",
        "Operating System :: OS Independent",
    ],
    "packages": [
        "mdis",
    ],
    "package_dir": {
        "": "src",
    },
}

if __name__ == "__main__":
    setup(**project)
